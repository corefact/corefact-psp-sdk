require 'faraday'


require 'psp/base'
require 'psp/order'
require 'psp/item'
require 'psp/address'
require 'psp/customer'

module Psp
  VERSION = '0.0.1'
  class Config
    class << self
     attr_accessor :url,
                  :api_key
      def uri
        @uri ||= Pathname.new(url)
      end

      def adapter
        Faraday.new do |builder|
          builder.use AuthTokenMiddleWare
          builder.use Faraday::Adapter::NetHttp
        end
      end

      def configure(&block)
        block.call(self)
      end
    end

    class AuthTokenMiddleWare < Faraday::Middleware
      def call(env)
        env[:request_headers].merge!('X-Auth-Token' => Psp::Config.api_key)
        @app.call(env)
      end
    end
  end
end

