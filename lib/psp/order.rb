require 'forwardable'
module Psp
  class Order < Base
    extend Forwardable
    # ShippingMethod = Struct.new(:carrier, :service)

    PriorityStandard  = 'standard'
    PriorityExpedited = 'expedited'
    PriorityLightning = 'lightning'
    PriorityReprint   = 'reprint'


    self.resource_path   = "vendor/v1/orders/%{order_id}"

    def_delegator :@items, :<<

    attr_accessor :order_id,                
                  :items,
                  :ship_address,
                  :carrier,
                  :service,
                  :priority,
                  :type,
                  :vendor

    attr_reader :has_valid_address, :state, :error_message, :tracking_number

    def initialize(order_id, customer, vendorInfo = {})
      @order_id         = order_id.to_s
      @customer         = customer
      @vendor           = { :account_name => 'standard' }.merge(vendorInfo)
      @items            = []

      @carrier         = 'UPS'
      @service         = 'GROUND'
    end

    def shipping_info(carrier, service)
      @carrier = carrier
      @service = service
    end

    # Api Mapping (Im explicity mapping this out for the purpose of the SDK)
    # else you could use a OStruct or attributes hash like active record
    def map_api_json_to_object(json)
      attribs = json.is_a?(Array) ? json.first : json

      @type             = attribs['type']
      @order_id         = attribs['order_id']
      @carrier          = attribs['carrier']
      @service          = attribs['service']
      @priority         = attribs['priority'] || PriorityStandard
      @customer     

      @items            = (attribs['items'] || []).map{ |item| Psp::Item.map_api_json_to_object(item) }
    end

    def to_api_json
      [as_json.except('error', 'status')].to_json
    end
  end
end


#  Print::Order.last.to_psp_order.save!