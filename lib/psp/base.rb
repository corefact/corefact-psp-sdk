require 'uri'
require 'json'

module Psp
  # Base resource for all our API calls
  RequestError = Class.new(StandardError)

  class Base
    # this is dupped will rewrite to cleanup

    # Default readonly attributes 
    attr_reader :created_at, :updated_at, :id

    def map_api_json_to_object(json)
      @id          = json['id'] if json['id']
      @created_at  = json['created_at'].to_time
      @updated_at  = json['updated_at'].to_time
    end

    ############
    # Helpers
    ############

    def save
      save rescue false
    end

    def save!
      return update! unless new_record?
      create!
    end

    def new_record?
      created_at.blank?
    end

    def update!
      wrap_request(Psp::Config.adapter.put(resource_url, to_api_json)) do |json|
        map_api_json_to_object(collection(json))
      end
      self
    end

    def create!
      wrap_request(Psp::Config.adapter.post(resource_url, to_api_json)) do |json|
        map_api_json_to_object(collection(json))
      end
      self
    end

    def resource_path
      self.class.hash_replace(self.class.resource_path.to_s, as_json)
    end

    def resource_url
      self.class.resource_url(resource_path)
    end

    def wrap_request(resp, &block)
      self.class.wrap_request(resp, &block)
    end

    def collection(json)
      self.class.collection(json)
    end

    def to_api_json
     to_json
    end

    class << self
      attr_accessor :resource_path, :collection_name

      def find(id)
        wrap_request(Psp::Config.adapter.get(resource_url(:order_id => id.to_s))) do |json|
          return nil if json.empty?
          map_api_json_to_object(collection(json).first)
        end
      end

       def map_api_json_to_object(json)
        rec = allocate
        rec.map_api_json_to_object(json)
        rec
      end

      # Ported for 1.8.7 functionality
      def hash_replace(str, hash_args)
        str.gsub(/%\{(.*?)\}/) { hash_args[$1] || hash_args[$1.to_sym] }    
      end


      def collection(json)
        collection_name.nil? ? json : json[collection_name]
      end

      # URL Stuff And helpers to make the implementation cleaner in the actual API

      # resource_url takes things and formats them to /resource/{id} allowing us to replace 
      # using the attributes of the instance (This is just for easy to read implementation)
      def resource_url(opts = {}, *pieces)
        url = "#{Psp::Config.url}#{resource_path}".gsub(/\/?$/, '')

        ps = pieces.compact
        url = [url, *ps].join("/") if ps.length > 0

        hash_replace(url.to_s, opts).gsub(/\/?$/, '')
      end

      def wrap_request(resp, &block)
        unless resp.status.between?(200, 302)
          raise RequestError, "Received status: #{resp.status} #{resp.body}"
        end
        json = JSON.parse(resp.body)
        block.call(json)
      end
    end
  end
end
