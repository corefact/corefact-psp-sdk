module Psp
  class File
    TYPE_ENVELOPE = 'envelope'
    TYPE_ORIGINAL = 'original'
    TYPE_MERGED   = 'merged'

    attr_accessor :type, 
                  :url

    attr_reader :format

    def initialize(type, url)
      @type = type
      @url = url
    end

    def map_api_json_to_object(json)
      @type   = json['type']
      @format = json['format']
      @url    = json['url']
    end
  end
end