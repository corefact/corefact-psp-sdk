module Psp
  class Customer < Base
    include Address

    def initialize(name, street1, city, state, zip)
      @name = name
      @street1 = street1
      @city  = city
      @state = state
      @zip   = zip
      @country = 'US'
    end


     # Api Mapping (Im explicity mapping this out for the purpose of the SDK)
    # else you could use a OStruct or attributes hash like active record
    def map_api_json_to_object(json)
      attribs = json.is_a?(Array) ? json.first : json
      super(attribs)
      attribs.each do |k,v|
        instance_variable_set("@#{k}", v)
      end
    end
  end
end