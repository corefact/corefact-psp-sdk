module Psp
  module Address
    attr_accessor :name, 
        :name2,
        :phone,
        :email,
        :street1,
        :street2,
        :city,
        :state,
        :zip,
        :country,
        :longitude,
        :latitude
  end

 
end