require 'psp/file'


module Psp
  class Item < Psp::Base
     extend Forwardable

    def_delegator :@files, :<<

    self.resource_path = "/vendors/v1/orders/%{order_id}/items/%{item_id}"

    attr_accessor :description, 
                  :sku,
                  :product,
                  :files,
                  :target_at,
                  :quantity,
                  :item_id

    attr_reader :product_id, 
                :state,
                :error

    def initialize(item_id, description, sku, quantity, target_at = nil)
      map_api_json_to_object({})

      @item_id     = item_id.to_s
      @description = description
      @sku         = sku
      @quantity    = quantity
      @files       = files || []
    end

    def map_api_json_to_object(json)
      @description = json['description']
      @sku         = json['sku']
      @files       = (json['files'] || []).map{|f| Psp::File.new(f[:type], f[:url]) }
      @quantity    = json['quantity']
      @target_at   = json['target_at'].to_time rescue nil # Eat this error

      @state       = json['state']  
      @error       = json['status']
    end
  end
end