require 'spec_helper'

SHOW_RESP_JSON = <<-JSON
{
  "orders": [
      {
          "carrier": "FEDEX",
          "created_at": "2019-01-11T16:17:36.388579-08:00",
          "has_valid_address": true,
          "id": 1,
          "items": [
              {
                  "created_at": "2019-01-11T16:17:36.389246-08:00",
                  "description": "Human Readable Description",
                  "die_cut_pattern": "None",
                  "die_cut_pattern_id": 16,
                  "external_id": "12345678-1",
                  "external_type": "brandname",
                  "files": [
                      {
                          "base_url": "http://example.com/mail.pdf",
                          "format": ".pdf",
                          "id": 1,
                          "type": "merged"
                      }
                  ],
                  "height": 0,
                  "id": 1,
                  "order_id": 1,
                  "paper": "",
                  "priority": "",
                  "product": {
                      "finishing": {
                          "color_scheme": "4/0",
                          "creased": false,
                          "height": 5.5,
                          "id": 58,
                          "paper": {
                              "code": "A2 -WHT1706",
                              "id": 15
                          },
                          "uv_type": "none",
                          "width": 4
                      },
                      "id": 31,
                      "name": "name",
                      "sku": "sku",
                      "type": "envelope",
                      "weight": 0.032
                  },
                  "product_id": 31,
                  "quantity": 26,
                  "sku": "sku",
                  "state": "created",
                  "status": "",
                  "target_at": "2019-01-14T16:17:36.389218-08:00",
                  "updated_at": "2019-01-11T16:17:36.389246-08:00",
                  "width": 0
              }
          ],
          "order_id": "12345678",
          "priority": "expedited",
          "service": "STANDARD_OVERNIGHT",
          "ship_address": {
              "city": "Roseville",
              "country": "US",
              "email": "",
              "name": "Tom Duder",
              "name2": "",
              "phone": "",
              "state": "CA",
              "street1": "1512 Eureka Rd #207",
              "street2": "",
              "zip": "95661"
          },
          "state": "created",
          "status": "",
          "target_at": "2019-01-14T16:17:36.388555-08:00",
          "tracking_number": "",
          "type": "brandname",
          "updated_at": "2019-01-11T16:17:36.388579-08:00",
          "weight": 0
      }
  ]
}
JSON

EXPECTED_CREATE_JSON = <<-JSON
 {
    "orders": [
        {
            "type": "BrandName",
            "order_id": "12345678",
            "priority": "expedited",
            "ship_address": {
                "name": "Tom Duder",
                "street1": "1512 Eureka Rd #207",
                "city": "Roseville",
                "state": "CA",
                "zip": "95661",
                "country": "US"
            },
            "carrier": "fedex",
            "service": "STANDARD_OVERNIGHT",
            "items": [
                {
                    "item_id": "1",
                    "sku": "sku",
                    "die_cut_pattern": "None",
                    "quantity": 26,
                    "description": "Human Readable Description",
                    "files": [
                        {
                            "type": "merged",
                            "url": "http://example.com/mail.pdf",
                            "format": ".pdf"
                        }
                    ]
                }
            ]
        }
    ]
}
JSON

CREATE_RESP_JSON = <<-JSON
{
    "orders": [
        {
            "carrier": "FEDEX",
            "created_at": "2019-01-11T16:17:36.388579-08:00",
            "has_valid_address": true,
            "id": 1,
            "items": [
                {
                    "created_at": "2019-01-11T16:17:36.389246-08:00",
                    "description": "Human Readable Description",
                    "die_cut_pattern": "None",
                    "die_cut_pattern_id": 16,
                    "external_id": "12345678-1",
                    "external_type": "brandname",
                    "files": [
                        {
                            "base_url": "http://example.com/mail.pdf",
                            "format": ".pdf",
                            "id": 1,
                            "type": "merged"
                        }
                    ],
                    "height": 0,
                    "id": 1,
                    "order_id": 1,
                    "paper": "",
                    "priority": "",
                    "product": {
                        "finishing": {
                            "color_scheme": "4/0",
                            "creased": false,
                            "height": 5.5,
                            "id": 58,
                            "paper": {
                                "code": "A2 -WHT1706",
                                "id": 15
                            },
                            "uv_type": "none",
                            "width": 4
                        },
                        "id": 31,
                        "name": "name",
                        "sku": "sku",
                        "type": "envelope",
                        "weight": 0.032
                    },
                    "product_id": 31,
                    "quantity": 26,
                    "sku": "sku",
                    "state": "created",
                    "status": "",
                    "target_at": "2019-01-14T16:17:36.389218-08:00",
                    "updated_at": "2019-01-11T16:17:36.389246-08:00",
                    "width": 0
                }
            ],
            "order_id": "12345678",
            "priority": "expidited",
            "service": "STANDARD_OVERNIGHT",
            "ship_address": {
                "city": "Roseville",
                "country": "US",
                "email": "",
                "name": "Tom Duder",
                "name2": "",
                "phone": "",
                "state": "CA",
                "street1": "1512 Eureka Rd #207",
                "street2": "",
                "zip": "95661"
            },
            "state": "created",
            "status": "",
            "target_at": "2019-01-14T16:17:36.388555-08:00",
            "tracking_number": "",
            "type": "brandname",
            "updated_at": "2019-01-11T16:17:36.388579-08:00",
            "weight": 0
        }
    ]
}
JSON

order_stubs = Faraday::Adapter::Test::Stubs.new do |stub|
  stub.get('/test.localvendor/v1/orders/12345678') { |env| [200, {}, SHOW_RESP_JSON] }
  stub.post('/test.localvendor/v1/orders') { |env| [200, {}, CREATE_RESP_JSON] }
end

describe Psp::Order do
  before(:each) do 
    Psp::Config.should_receive(:adapter).and_return do 
      Faraday.new{ |builder| builder.adapter(:test, order_stubs) }
    end
  end

  let(:order) do  
    Psp::Order.new(12345678, 
      'BrandName', 
      { :name => 'Test Customer', 
        :street1 => '1234 Main St.', 
        :city => 'Rosevile', 
        :state => 'CA', 
        :zip => '95661' })
  end

  let(:item) {
     Psp::Item.new(1, '1 Sided Flyer', '1SFLYER', 1, Time.now, [ Psp::File.new(Psp::File::TYPE_MERGED, 'http://someurl/proof.pdf') ])
  }

  it 'should unmarshal the response into an order object' do  
    order = Psp::Order.find(12345678)
    order.order_id.should == "12345678"

    order.priority.should == Psp::Order::PriorityExpedited

    order.items.length.should == 1

    item = order.items.first 
    item.is_a?(Psp::Item).should be_true
    item.files.length.should == 1
  end

  it 'should create an order' do 
    lambda { order.create! }.should_not raise_error
    order.created_at.should != nil
  end
end
