require 'spec_helper'

describe Psp::Base do
 class O < Psp::Base
    self.resource_path = '/some/path/%{id}'
 end

  it 'should create the proper urls' do 
    O.send(:resource_url).should eql "test.local/some/path"
  end
  it 'should replace out URLS given a hash' do 
    O.send(:resource_url, :id => 1).should eql "test.local/some/path/1"
  end
end