require 'psp'
require 'pry'
require 'thread'
unless defined?(ActiveSupport)
  require 'activesupport'
end


Psp::Config.configure do |config|
  config.url = "test.local"
  config.api_key = "TESTKEY"
end