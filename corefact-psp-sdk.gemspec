$LOAD_PATH.unshift 'lib'

require 'psp'

Gem::Specification.new do |s|
  s.name = "corefact-psp-sdk"
  s.version = Psp::VERSION
  s.date = Time.now.strftime('%Y-%m-%d')
  s.summary = "Integrates with Corefact's PSP System"
  s.authors = ["Mitch Rodrigues"]
  s.require_paths = ["lib"]
  s.email = "mitch@corefact.com"
  s.files = `git ls-files`.split("\n")
  s.test_files = `git ls-files -- {test}/*`.split("\n")
  s.homepage = "http://corefact.com"

  s.description = "Corefact PSP Ruby Implementation"

  s.add_runtime_dependency 'activesupport'
  s.add_runtime_dependency 'faraday'
  s.add_runtime_dependency 'json'
end